<!DOCTYPE html>
<html>
  <head>
    <title>Belatih HTML</title>
  </head>

  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      First name:<br><br>
      <input id="first_name" name="first_name" type="text"></input>
      <br><br>

      Last name:<br><br>
      <input id="last_name" name="last_name" type="text"></input>
      <br><br>

      Gender:<br><br>
      <input type="radio" id="gender" name="gender" value="male">Male</input><br>
      <input type="radio" id="gender" name="gender" value="female">Female</input><br>
      <input type="radio" id="gender" name="gender" value="other">Other</input>
      <br><br>

      Nationality:<br><br>
      <select id="nationality" name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="other">Other</option>
      </select>
      <br><br>

      Language Spoken:<br><br>
      <input type="checkbox" id="language1" name="language[]" value="bahasa_indonesia">
      <label for="language1"> Bahasa Indonesia</label><br>
      <input type="checkbox" id="language2" name="language[]" value="english">
      <label for="language2"> English</label><br>
      <input type="checkbox" id="language3" name="language[]" value="other">
      <label for="language3"> Other</label>
      <br><br>

      Bio:<br><br>
      <textarea id="bio" name="bio" rows="8" cols="30"></textarea>
      <br>
      <input type="submit" value="Sign Up"></input>
    </form>
  </body>
</html>
